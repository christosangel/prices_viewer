//┏━┓┏━┓╻┏━╸┏━╸   ╻ ╻╻┏━╸╻ ╻┏━╸┏━┓
//┣━┛┣┳┛┃┃  ┣╸    ┃┏┛┃┣╸ ┃╻┃┣╸ ┣┳┛
//╹  ╹┗╸╹┗━╸┗━╸╺━╸┗┛ ╹┗━╸┗┻┛┗━╸╹┗╸
//written by Christos Angelopoulos, 1636727442
const x = document.querySelector("input");
x.addEventListener("change", () =>
  {
    const fr = new FileReader();
    fr.onloadend = e => {
        let r = fr.result.split("\n").map(e => {
            return e.split(",");
        });
        r.forEach(e => {
            let m = e.map(e => {
                return `<td>${e}</td>`;
            }).join("");
            const ce = document.createElement("tr");
            ce.innerHTML = m;

            if (ce.innerText !== "") {
                document.querySelector("table.times").append(ce);

            }

        });

        const epik1cell = document.createElement("td");
        let epik1 = document.querySelector('.times tr:nth-child(1) td:nth-child(1)');
        epik1cell.innerText = epik1.innerText
        document.querySelector("table.epikefalida tr").append(epik1cell);

        const epik2cell = document.createElement("td");
        let epik2 = document.querySelector('.times tr:nth-child(1) td:nth-child(2)');
        epik2cell.innerText = epik2.innerText
        document.querySelector("table.epikefalida tr").append(epik2cell);

        const epik3cell = document.createElement("td");
        let epik3 = document.querySelector('.times tr:nth-child(1) td:nth-child(3)');
        epik3cell.innerText = epik3.innerText
        document.querySelector("table.epikefalida tr").append(epik3cell);

        const epik4cell = document.createElement("td");
        let epik4 = document.querySelector('.times tr:nth-child(1) td:nth-child(4)');
        epik4cell.innerText = epik4.innerText
        document.querySelector("table.epikefalida tr").append(epik4cell);

        const epik5cell = document.createElement("td");
        let epik5 = document.querySelector('.times tr:nth-child(1) td:nth-child(5)');
        epik5cell.innerText = epik5.innerText
        document.querySelector("table.epikefalida tr").append(epik5cell);

        // console.log(r)
    }
    fr.readAsText(x.files[0]);
  }
)
