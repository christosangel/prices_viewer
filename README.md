# prices_viewer

This a pretty simple html-css-js app that helps you view prices.csv created by desfa_prices, in a browser tab.
 
## INSTALL (Windows)

Visit [https://gitlab.com/christosangel/prices_viewer](https://gitlab.com/christosangel/prices_viewer):

 - click the **download** button,
 - select **zip** and
 - download **prices_viewer-main.zip**
 - Extract **prices_viewer-main** folder from zip file

---
  
![1.png](screenshots/1.png)

---





### CREATE A SHORTCUT for **prices_viewer.html**


Right-click on **prices_viewer.html** and select *Create Shortcut*.

Right-click on  **Shortcut of prices_viewer.html**
 

 * select *Properties*:


  On tab *Shortcut*:  

 - Click *Change Icon*,

 - select *Browse*, 

 - navigate to **prices_viewer-main** folder, 

 - select **prices_viewer.ico**

 - click *Open*.


 On tab *General*:

 -   *Add name* of your liking
 -  *Open with* : add the name of your prefered browser
 -  Click *OK*


That's it!

---

### RUN

Navigate to your desktop, double-click on created shortcut.

A new tab will open in your browser.

 - Click on **Choose File** button, 
 - navigate to **desfa_prices** folder,
 - select **prices.csv**
 - click *Open*
 
---

![2.png](screenshots/2.png)

---

The selected csv file is presented in this tab:

---

![3.png](screenshots/3.png)


---
















